import React, {Component} from 'react'
import { View, Text} from 'react-native'
import { Route, NativeRouter, Switch,Redirect } from 'react-router-native'
import Screen1 from './Screen1'
import Screen2 from './Screen2'
import LoginPage from './LoginPage'
import ProfilePage from './ProfilePage'
import component1 from './component1'

class Router extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    <Route exact path='/Screen1' component={Screen1}/>
                    <Route exact path='/Screen2' component={Screen2}/>
                    <Route exact path='/LoginPage' component={LoginPage}/>
                    <Route exact path='/ProfilePage' component={ProfilePage}/>
                    <Route exact path='/component1' component={component1}/>
                    <Redirect to="/LoginPage" />
                </Switch>
            </NativeRouter>
        )
    }
}
export default Router