import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Button, Alert } from 'react-native';

class ProfilePage extends Component {
    UNSAFE_componentWillMount() {
        console.log(this.props)
    }
    goToScreen1 = () => {
        this.props.history.push('/LoginPage')
    }
    render() {
        return (
            <View style={[styles.container]}>
                <View style={[styles.header, styles.center]}>
                    <Text style={styles.textHeader}> ProfilePage </Text>
                </View>
                <View style={[styles.content]}>
                    <Text style={styles.textHead}> Username </Text>
                    <Text style={styles.text}>: {this.props.location.state.username}</Text>
                    <Text style={styles.textHead}> First name </Text>
                    <Text style={styles.textHead}> Last name</Text>
                </View>
                <View style={[styles.footer]}>
                    <Button title="Edit" />
                    <Button title="Go to Login" onPress={this.goToScreen1} />
                </View>

            </View>


        )
    }
}
export default ProfilePage

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F08080',
        flex: 1
    },
    textHeader: {
        fontSize: 40,
        textAlign: 'center',
        margin: 10,
    },
    textHead: {
        fontSize: 30,
        color: 'white',
        margin: 10,
    },
    text: {
        fontSize: 20,
        color: 'white',
        left: 30,
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        backgroundColor: '#FADBD8',
    },
    content: {
        flex: 1,
    },
    footer: {
        backgroundColor: '#FADBD8',
    },
});

