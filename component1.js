import React, {Component} from 'react'
import { View, Text,Alert,Button} from 'react-native'
import { Link } from 'react-router-native'
import styled from 'styled-components'

class component1 extends Component {
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#F08080' }}>
            <Text style={{ color: 'white' }}> Screen 1 </Text>
            <Link to="/Screen2">
                <MyButton title='Go to Screen 2'/>
            </Link>
        </View> 
        )
    }
}

const MyButton = styled.Button`
  background: transparent;
  border-radius: 3px;
  border: 2px solid palevioletred;
  color: palevioletred;
  margin: 0 1em;
  padding: 0.25em 1em;
`

export default component1


  