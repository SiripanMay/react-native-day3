import React, {Component} from 'react'
import { View, Text,Button} from 'react-native'

class Screen2 extends Component {
    goToScreen1=()=>{
        this.props.history.push('/Screen1',{
            mynumber:20 //ส่งขอ้มูลไปยัง Screen1
        })
    }
    render() {
        return (
            <View style={{flex: 1, backgroundColor: 'pink'}}>
            <Text style={{color:'white'}}> Screen 2 </Text>
            <Button title="Go to Screen1" onPress={this.goToScreen1}/>
            </View>
        )
    }
}
export default Screen2

